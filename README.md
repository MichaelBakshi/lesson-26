1.זוהי בעיה בקוד שמונעת ממנו להמשיך. כגון, חלוקה באפס. 
2. כל התכנית תקרוס
3. א. SYSTEMEXCEPTION
   ב. APPLICATIONEXCEPTION
ישנן חריגות מובנות בקוד וישנן חריגות שאנחנו נבנה
4. TRY נותן לנו אפשרות לבצע קוד
ולהימנע מקריסת תכנית. 
CATCH נותן לנו אפשרות לטפל בחריגה
5. לא, התכנית תעצור
6. בעזרת פקודה נוספת: FINALLY
7. לחריגות יש רמות. אם לא תפסתי חריגה ספציפית, אני יכול להגדיר חריגה רמה אחת או כמה מעליה כדי לדאוג שהחריגה תטופל בכל מקרה
8. כדי לראות את רצף הפונקציות שהובילו אותנו לחריגה.
9. ליצור מחלקה נפרדת שתירוש מ EXCEPTION
10. כדאי לפרט כמה שיותר כדי שיהיה ברור באיזו נקודה בקוד אני נמצא ומה גרם לחריגה
11. לא ניתן. TRY CATCH  תמיד חייבים לבוא בזוג
12. לשים BREAKPOINT 
בנקודה בה רוצים לעצור את ה DEBUGGER