﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_26
{
    class Program
    {
        static void Main(string[] args)
        {
            MyProtectedUniqueList uniqueList = new MyProtectedUniqueList("Pony");

            try
            {
                //uniqueList.Add("");
                //uniqueList.Add("Pony");
                uniqueList.Add("Resto");
                uniqueList.Add("Pesto");
                uniqueList.Add("Kadabra");
                uniqueList.Add("Abra");

                //uniqueList.Remove("Tiesto");
                //uniqueList.Remove("");
                uniqueList.Remove("Resto");

                //uniqueList.RemoveAt(5);
                uniqueList.RemoveAt(0);

                //uniqueList.Sort("trtr");
                uniqueList.Sort("Pony");

                //uniqueList.Clear("ooop");
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentNullException e2)
            {
                Console.WriteLine(e2.Message);
            }
            catch (ArgumentOutOfRangeException e4)
            {
                Console.WriteLine(e4.Message);
            }
            catch (ArgumentException e3)
            {
                Console.WriteLine(e3.Message);
            }
            catch (AccessViolationException e5)
            {
                Console.WriteLine(e5.Message);
            }

            Console.WriteLine(uniqueList.ToString());
            foreach (string item in uniqueList)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("Program reached end");
        }
    }
}
