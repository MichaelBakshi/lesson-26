﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_26
{
    class MyProtectedUniqueList: IEnumerable<string>
    {
        private List<string> words = new List<string>();
        private string secretWord ;

        public   MyProtectedUniqueList(string s)
        {
            this.secretWord = s;
        }
        public void Add(string s)
        {
            
                if (s == null || s.Length == 0)
                {
                    throw new ArgumentNullException("Your input is empty");
                }
                else if (s.Equals(secretWord))
                {
                    throw new InvalidOperationException("This word already exists in the list!");
                }
                else
                {
                    words.Add(s);
                }
            
            
        }
        public void Remove(string s)
        {
            if (s == null || s.Length==0)
            {
                throw new ArgumentNullException("Your input is empty");
            }
            else if (!words.Contains(s))
            {
                throw new ArgumentException("This word doesn't exist in the list!");
            }
            else if (words.Contains(s))
            {
                words.Remove(s);
            }
        }
        public void RemoveAt(int i)
        {
            if (i<0 || i>words.Count)
            {
                throw new ArgumentOutOfRangeException("Index is out of range!");
            }
            else
            {
                words.RemoveAt(i);
            }
        }
        public void Clear(string s)
        {
            if (s.Equals(secretWord))
            {
                words.Clear();
            }
            else
            {
                throw new AccessViolationException("You have no access to clear the list");
            }
        }
        public void Sort(string s)
        {
            if (s.Equals(secretWord))
            {
                words.Sort();
            }
            else
            {
                throw new AccessViolationException("You have no access to sort the list");
            }
        }

        public override string ToString()
        {
            string result = "";
            foreach (var item in words)
            {
                result +=  item + " \n";
            }
            return result;
        }

        public IEnumerator<string> GetEnumerator()
        {
            return this.words.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.words.GetEnumerator();
        }
    }
}
